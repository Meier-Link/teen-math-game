import { Entity } from './entity';

export class Level {
    /* Unique number which show when the level will be launched */
    order: number;
    /* A French name for this level of the game */
    name: string;
    /* The PNG file location */
    mapAsset: string;
    /* Content of the map, stored with coordinates */
    mapEntities : Entity[];

    constructor(order: number, name: string, mapAsset: string) {
        this.order = order;
        this.name = name;
        this.mapAsset = mapAsset;
    }

    registerEntity(entity: Entity) {
        // TODO additional checks
        this.mapEntities.push(entity);
    }
}
