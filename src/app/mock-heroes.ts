import { Hero } from './hero';

export const HEROES: Hero[] = [
    new Hero('Ike', 'warrior', 39, 35),
    new Hero('Cecilia', 'warrior', 33, 39),
    new Hero('Daraen', 'alchemist', 30, 31),
    new Hero('Tiki', 'alchemist', 32, 29)
];

export const NPCS = {
    'Charles Babbage': new Hero('Arvis', 'scientist', 0, 0),
    'George Boole': new Hero('Camus', 'scientist', 0, 0),
    'Augustin-Louis Cauchy': new Hero('Reinhardt', 'scientist', 0, 0),
    'Euclid': new Hero('Wrys', 'scientist', 0, 0),
    'Chimera': new Hero('Chimera', 'monster', 0, 0),
    'Pythagoras': new Hero('Shigure', 'scientist', 0, 0),
    'Zhang Heng': new Hero('Soren', 'scientist', 0, 0),
    'Blaise Pascal': new Hero('Matthew', 'scientist', 0, 0),
    'Laevatein': new Hero('Laevatein', 'villain', 0, 0),
    'Laevatein-Injured': new Hero('Laevatein-Injured', 'villain', 0, 0),
    // Gottfried Wilhelm Leibniz (Jakob)
};
