import { Chapter } from '../chapter';
import { Event } from '../event';
import { NPCS } from '../mock-heroes';

// Hero
let event1Content = "<p>Bien, nous voici arrivé à Alexandrie...</p>"
    + "<p>Maintenant, il va falloir identifier et retrouver la distorsion ...</p>";
// Euclid
let event2Content = "<p>Help !<br/>Ohé !<br/>Quelqu'un peu m'aider?</p>"
    + "<p>Oui, c'est moi qui ait appelé à l'aide... Ici, juste au dessus de vous...</p>";
// Hero
let event3Content = "<p>Mais ... Vous êtes 'dans' la ville, là, non ?</p>"
    + "<p>... C'est juste que vous êtes au dessus de nos têtes, comme si vous voliez !</p>";
// Hero
let event4Content = "<p>Quel étrange phénomène ...</p>";
// Euclid
let event5Content = "<p>Je ne vous le fait pas dire!</p>"
    + "<p>Mais vous pourriez peut-être m'aider ... En quelle année sommes nous ? Où sommes nous ?</p>";
// Hero
let event6Content = "<p>C'est vrai que ça ne doit pas être évident ... Nous sommes en 1845, à Alexandrie</p>";
// Euclid
let event7Content = "<p>1845 ? Mais de quel époque ?</p><p>Tout semble si ... Différend de mon époque...</p>";
// Hero
let event8Content = "<p>Alors là, vous me poser une colle...</p><p>Vous pouvez nous dire qui vous êtes ? Peut-être qu'on "
    + "pourra vous répondre.";
// Euclid
let event9Content = "<p>Peut-être bien, oui. Je m'appelle Euclide.</p>"
    + "<p>Je suis un mathématicien grec qui a écrit de nombreux théorèmes sur la géométrie et l'arithmétique.</p>"
    + "<p>De ce que j'en sais, mon nom est resté dans l'histoire, et les étudiants entendent parler de choses "
    + "comme la division euclidienne, la géométrie euclidienne, ...</p>";
// Hero
let event10Content = "<p>Effectivement, cela nous aide bien... Et je crois que vous êtes très loin de chez vous, "
    + "ou plutôt de votre époque !</p>"
    + "<p>Nous sommes quelque chose comme 2500 ans après la période où vous avez vécu.</p>";
// Euclid
let event11Content = "<p>Vraiment ? Eh bien, que le monde a changé ... Je ne l'aurai jamais cru !</p>"
    + "<p>Bon, par contre, je rentrerai volontiers à mon époque ... Et puis ce n'est pas désagréable, de flotter, comme ça, "
    + "mais je dois dire que le plancher des vaches me manque ...</p>"
    + "En arrivant ici, j'ai remarqué une créature qui ressemblait à une chimère de nos légendes, vous savez ? "
    + "Avec une tête de lion, une queue de serpent ...</p>"
    + "<p>Ah bah tenez, la voilà qui arrive.</p>";
// Chimera
let event12Content = "<p>Qui êtes vous ? Comment avez-vous pu vous approcher de cet individu ?</p>"
    + "<p>Je dois empêcher quiconque de l'approcher ... Si vous voulez le délivrer, il faudra répondre à mes énigmes, "
    + "sinon, je vous dévorerai...</p>";
// Euclid
let event13Content = "<p>Ah ? Bah tien, c'est commes les chimères de nos légendes ...</p>";
// Chimera
let event14Content = "<p>Normal, j'ai été créé d'après vos fameuses légendes ... Mais trêve de plaisanterie, "
    + "j'ai une première question pour vous, les petits ...</p><p>Dites-moi combien font 2 876 + 19 + 148 ?</p>";
// Euclid
let event15Content = "<p>Attend donc ! Nous devrions quand même leurs donner quelques outils, histoire d'avoir une "
    + "chance de répondre juste ...</p>";
// Chimera
let event16Content = "<p>Humpf, je ne vois nulle part dans mon contrat quelque chose qui l'empêcherait ...</p>";
// Euclid
let event17Content = "<p>Bien bien bien, alors voici quelques petites informations pour vous permettre de résoudre "
    + "les énigmes de madame la chimère que voici.</p>";
// Chimera
let event18Content = "<p>Mademoiselle...</p>";
// Euclid
let event19Content = "<p>Pardonnez moi...</p><p>Bref, où en étions-nous ... Ah oui, les additions et les soustractions.</p>";
// Hero
let event20Content = "<p>Ah, je me souviens déjà de quelques petites choses ...<p>"
    + "<p>Si je ne me trompe pas, pour additionner ou soustraire plusieurs nombres, on doit les <b>les aligner par catégorie</b> "
    + "en partant des <b>unités</b></p>"
    + "<p>Par exemple, si on reprend la question de la chimère, 2 876 est proche de 3 000, 19 est proche de 20, "
    + "et 148 est proche de 150 !<br/>"
    + "Donc le vrai résultat doit être proche de 3 170.</p>";
// Chimera
let event21Content = "<p>Oui, mais ce n'est pas exactement le résultat que j'attendait...</p>";
// Hero
let event22Content = "<p>C'est vrai... Donc, on doit trouver le résultat de  2 876 + 19 + 148, "
    + "et on sait qu'il doit être proche de 3 170 ...</p>"
    + "<p>Du coup, combien est le résultat exact ?</p>";
let event22Answer = "3 043";
// Chimera
let event23Content = "<p>Mouais bon ... Vous avez été aidés, aussi...</p><p>Essayez plutôt celle-ci : combien font "
    + "49 725 + 3 024 + 685 ?</p>";
let event23Answer = "53 434";
// Chimera
let event24Content = "<p>Mouais ... On va voir comment vous vous en sortez avec les soustractions, maintenant...</p>"
    + "<p>Combien font 8 235 - 4 542 ?</p>";
let event24Answer = "3 693";
// Chimera
let event25Content = "<p>Attendez ! Attendez ... Elle était trop simple, celle-là ... En voici une autre ...</p>"
    + "<p>Combien font 465 286 - 281 379 ?</p>";
let event25Answer = "183 907";
// Chimera
let event26Content = "<p>Grmbl ... On va passer la vitesse supérieur, alors, avec les multiplications.</p>";
// Euclid
let event27Content = "<p>Ah mais attendez, il ne sont pas équipés, pour résoudre des multiplications ... "
    + "Du moins pas encore.</p>"
    + "<p>Alors, pour multiplier des nombres entre eux, vous devez <b>respecter</b> l'ordre des <b>catégories</b> "
    + "en <b>commençant</b> par les <b>unités</b>. Cela revient à <b>décomposer</b> le multiplicateur.</p>"
    + "<p>Par exemple, 435 X 1<b>3</b>2 = (435 X 100) + (435 X <b>30</b>) + (435 X 2).</p>"
    + "<p>Quand vous posez l'opération, vous appliquez cette <b>décomposition</b>.</p>";
// Hero
let event28Content = "<p>Ah je me souviens, ça ressemble à la décomposition des grands nombres qu'on avait vu avec "
    + "George Boole, Charles Babbage et Augustin-Louis Cauchy ! Tu sais, les trois scientifiques qui nous ont envoyé "
    + "ici !</p>";
// Hero
let event29Content = "<p>Ah oui, je m'en souviens, on s'en servait pour identifier les <b>classes</b> des grands nombres.";
// Euclid
let event30Content = "<p>Effectivement, c'est un excellent outil pour manipuler les grands nombres, et comme "
    + "vous allez le voir, ça aide aussi pour faire des opérations dessus.</p>"
    + "<p>Histoire que le vocabulaire soit bien clair, pour vous, dans la multiplication 435 X 32 = 13 920, <b>435</b> et "
    + "<b>32</b> sont les <b>facteurs</b>, <b>32</b> est le <b>multiplicateur</b>, et <b>13290</b> est le <b>produit</b>.</p>"
    + "<p>Quand vous commencez une ligne, vous <b>alignez</b> le résultat avec la catégorie du multiplicateur :</p>"
    + "<p><img src=\"assets/img/resources/multiplication1.jpg\" /></p>"
    + "<p>Pour vous faciliter la tâche, pensez à faire un <b>calcul approché</b> avant de commencer.</p>";
// Chimera
let event31Content = "<p>Peuh ! Tu lui mâche tout le travail !</p>"
    + "<p>Enfin, on va v oir si vous avez compris les explications d'Euclide...</p>"
    + "<p>Retrouvez moi la multiplication décomposée de (527 X 100) + (527 X 30) + (527 X 6)</p>";
let event31Answer = "527 X 136";
// Chimera
let event32Content = "<p>Mouais ... Et maintenant, ça fait combien, 527 X 136 ?</p>";
let event32Answer = "71 672";
// Chimera
let event33Content = "<p>Rhaaa C'était encore trop simple, c'est ça ?</p>"
    + "<p>Soit, essayez donc de résoudre la multiplication suivante :<br/>"
    + "5208 X 437</p>";
let event33Answer = "2 275 896";
// Chimera
let event34Content = "<p>Ok, vous ne vous êtes pas fait avoir par le piège du zéro.</p>"
    + "<p>Trouvez donc la solution à mes deux énigmes, et je vous laisserai délivrer Euclide...</p>"
    + "<p>Voici ma première énigme :<br/>"
    + "Les habitants d'un village on extrait 11 500 000 pierres précieuses d'une grotte en 10 jours.<br/>"
    + "Ils en avaient trouvé 8 300 000 au cours des neuf premiers jours.<br/>"
    + "Combien ont-ils rapportés de pierre précieuses le dernier jour ?</p>";
let event34Answer = "3 200 000";
// Chimera
// TODO distance Paris Moscou
let event35Content = "<p>Bien vu... Alors voici la seconde énigme :<br/>"
    + "Un cavalier peut par courir 350 kilomètres par semaine.<br/>"
    + "Sera-t-il capable de ralier Moscou depuis Lisbone en moins de 15 semaines, sachant que les deux villes "
    + "sont distantes de 5 170km ?</p>"
    + "<p>Répondez par 'oui' ou 'non'.</p>";
let event35Answer = "oui";
// Euclid
let event36Content = "<p>Bravo ! Vous avez trouvé toute les réponses !</p>"
    + "<p>Sur la fin, quand je vous voyait penché sur vos notes, je me suis fait un peu de soucis, mais vous avez "
    + "été à la hauteur !</p>"
    + "<p>Maintenant, je vais pouvoir retourner à mon époque ...</p>";
// Chimera
let event37Content = "<p>Laissez-moi faire, ça fait partie de mes attributions ...</p>";
// Hero
let event38Content = "<p>Très bien, nous allons pouvoir aller vers une nouvelle cité...<br/>"
    + "Nous aurons sans doute d'autres épreuves similaires pour libérer les autres scientifiques.</p>"
    + "<p>Par contre, nous ne savons toujours pas qu'est-ce qui les a amené ici ...</p>";

export const CHAPTER2 : Chapter = new Chapter("Alexandrie - les opérations de la chimère.",
    [
        new Event("Alexandrie", event1Content, null),
        new Event("Alexandrie", event2Content, NPCS['Euclid']),
        new Event("Alexandrie", event3Content, null),
        new Event("Alexandrie", event4Content, null),
        new Event("Alexandrie", event5Content, NPCS['Euclid']),
        new Event("Alexandrie", event6Content, null),
        new Event("Alexandrie", event7Content, NPCS['Euclid']),
        new Event("Alexandrie", event8Content, null),
        new Event("Alexandrie", event9Content, NPCS['Euclid']),
        new Event("Alexandrie", event10Content, null),
        new Event("Alexandrie", event11Content, NPCS['Euclid']),
        new Event("Alexandrie", event12Content, NPCS['Chimera']),
        new Event("Alexandrie", event13Content, NPCS['Euclid']),
        new Event("Alexandrie", event14Content, NPCS['Chimera']),
        new Event("Alexandrie", event15Content, NPCS['Euclid']),
        new Event("Alexandrie", event16Content, NPCS['Chimera']),
        new Event("Alexandrie", event17Content, NPCS['Euclid']),
        new Event("Alexandrie", event18Content, NPCS['Chimera']),
        new Event("Alexandrie", event19Content, NPCS['Euclid']),
        new Event("Alexandrie", event20Content, null),
        new Event("Alexandrie", event21Content, NPCS['Chimera']),
        new Event("Alexandrie", event22Content, null, [], [], event22Answer),
        new Event("Alexandrie", event23Content, NPCS['Chimera'], [], [], event23Answer),
        new Event("Alexandrie", event24Content, NPCS['Chimera'], [], [], event24Answer),
        new Event("Alexandrie", event25Content, NPCS['Chimera'], [], [], event25Answer),
        new Event("Alexandrie", event26Content, NPCS['Chimera']),
        new Event("Alexandrie", event27Content, NPCS['Euclid']),
        new Event("Alexandrie", event28Content, null),
        new Event("Alexandrie", event29Content, null),
        new Event("Alexandrie", event30Content, NPCS['Euclid']),
        new Event("Alexandrie", event31Content, NPCS['Chimera'], [], [], event31Answer),
        new Event("Alexandrie", event32Content, NPCS['Chimera'], [], [], event32Answer),
        new Event("Alexandrie", event33Content, NPCS['Chimera'], [], [], event33Answer),
        new Event("Alexandrie", event34Content, NPCS['Chimera'], [], [], event34Answer),
        new Event("Alexandrie", event35Content, NPCS['Chimera'], [], [], event35Answer),
        new Event("Alexandrie", event36Content, NPCS['Euclid']),
        new Event("Alexandrie", event37Content, NPCS['Chimera']),
        new Event("Alexandrie", event38Content, null),
    ]
);
