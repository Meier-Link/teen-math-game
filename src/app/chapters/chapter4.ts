import { Chapter } from '../chapter';
import { Event } from '../event';
import { NPCS } from '../mock-heroes';

// Hero
let event1Content = "<p>Nous sommes arrivé à Xi'an.</p>"
    + "<p>Il nous reste encore une bonne demi journée de route jusqu'à Nanyang, mais il semblerait "
    + "qu'il se soit également passé quelque chose, ici.</p>";
// Hero
let event2Content = "<p>Oui, les statuts de l'armée de l'empereur Qin ont été détruites par un "
    + "tremblement de terre.</p>";
// Hero
let event3Content = "<p>On va aller voir ça.</p>";
// Hero
let event4Content = "<p>Nous y voilà. Sur le chemin, des gens nous ont dit qu'il y avait comme une sorte "
    + "d'arène, au milieu.</p>"
    + "<p>Ce doit être ce bâtiment, là-bas. Je vois qu'il y a quelqu'un qui s'agite en haut.</p>";
// Heng
let event5Content = "<p>Bonjour jeunes gens. Je me nomme Zhang Heng.</p>"
    + "<p>Je suis un astronome, mathématicien et homme d'état du troisième siècle de la dynastie Han.</p>"
    + "<p>J'ignore ce qu'il s'est passé ici, mais c'est impressionnant de voir toute les statuts de "
    + "l'empereur Qin dans cet état ...</p>"
    + "<p>Mais d'après une stèle ici, il faut toute les rassembler pour que je retrouve ma liberté.</p>";
// Hero
let event6Content = "<p>Troisième siècle de la dynastie Han ... Ça signifie le premier siècle après "
    + "Jésus-Christ.</p>"
    + "<p>Pour les status, il va déjà falloir y mettre de l'ordre... Mais pour ça, on doit ranger "
    + "les têtes des statues par 12, les corps par 8, les jambes des chevaux par 4 ...</p>";
// Heng
let event7Content = "<p>Pour vous aider, je vais vous expliquer rapidement comment faire les divisions.</p>"
    + "<p>Prenons pour exemple l'expression suivante : 4205 : 82 = 52, reste 23.</p>"
    + "<ul>"
    + "<li>4205 est le <b>dividende</b></li>"
    + "<li>82 est le <b>diviseur</b></li>"
    + "<li>51 est le <b>quotient</b></li>"
    + "<li>23 est le <b>reste</b></li>"
    + "</ul>";
// Heng
let event8Content= "<p>La première étape d'une division consiste à retrouver le nombre de chiffres "
    + "qui composent le quotient.</p>"
    + "<p>Avec notre exemple, on pose <b>82 X 100</b> &lt; 4205 &lt; <b>82 X 1000</b>.</p>"
    + "<p>Cela nous permet d'écrire <b>10</b> &lt; q &lt; <b>100</b>. Donc q aura 2 chiffres.</p>"
    + "<p>Vous n'aurez donc à faire que deux divisions, dans ce cas là.</p>";
// Heng
let event9Content = "<p>Faire une division, c'est chercher le <b>facteur manquant</b> d'une "
    + "multiplication. Par exemple, si on a 4 X 2 = 8, on peut dire que 8 : 4 = 2 ou 8 : 2 = 4.</p>"
    + "<p>Afin de voir si vous avez bien compris, essayez de retrouver le nombre de chiffre du "
    + "quotient pour 139 : 25.</p>";
let event9Answer = "1";
// Heng
let event10Content = "<p>Bien, cet exemple était simple. On va essayer avec un autre plus complexe.</p>"
    + "<p>Combien y-a-t-il de chiffres au quotient pour 50 268 : 37 ?</p>";
let event10Answer = "4";
// Heng
let event11Content = "<p>Bravo ! Vous avez bien compris les bases.</p>"
    + "<p>On va reprendre notre premier exemple, c'est-à-dire 139 : 25.</p>"
    + "<p>Vous avez trouvé que le quotient comporte un chiffre. Essayez maintenant de retrouver "
    + "ce quotient</p>";
let event11Answer = "5";
// Heng
let event12Content = "<p>Très bien, maintenant, de combien est le reste de 139 : 25 ?</p>"
    + "<p>Pour cela, rappelez-vous que si le nombre a diviser n'est pas un multiple du diviseur "
    + "(ce qui est le cas ici), il va y avoir <b>un reste</b> car on va se servir du <b>multiple</b> "
    + "le plus proche.</p>"
    + "<p>Souvenez-vous qu'on a trouvé que 25 allait 5 fois dans 139. Je vous laisse retrouver "
    + "le reste avec ces informations.</p>";
let event12Answer = "14";
// Hero
let event13Content = "<p>C'est facile, c'est 14, la réponse !</p>"
    + "<p>Mais il nous faudrait une méthode pour gérer des divisions plus complexes...</p>";
// Heng
let event14Content = "<p>Effectivement, voici une bonne méthode à appliquer :</p>"
    + "<p><img src=\"assets/img/resources/divisions2.jpg\" /></p>"
    + "<ul>"
    + "<li>1) Vous allez faire une division <b>approchée</b> des dizaines. Ici, 42 : 5. 5  va 8 fois "
    + "dans 42.</li>"
    + "<li>2) Vous calculez ensuite le multiple, c'est-à-dire 5 X 82 = 410, et nous voyons bien que "
    + "410 &lt; 420.</li>"
    + "<li>3) Vous soustrayez ensuite ensuite le multiple du <b>dividende</b>, ce qui vous donnera un "
    + "premier reste, ici 10.</li>"
    + "<li>4) Ce reste est inférieur à 82, vous pouvez donc <b>abaisser</b> le 5 des unités, puis calculer "
    + "105 : 82 en appliquant la même méthode.</li>"
    + "<li>5) Tu peux ensuite vérifier ton résultat en faisant 4205 = (51 X 82) + 23</li>"
    + "</ul>";
// Hero
let event15Content = "<p>Ah oui, cette méthode me rappelle vaguement quelque chose...</p>"
    + "<p>Merci beaucoup ! Avec ça, nous devrions pouv oir nous en sortir avec les status de l'armée "
    + "de l'empereur Qin.</p>";
// Heng
let event16Content = "<p>Oui ... Cet empereur a laissé de mauvais souvenirs, à notre époque.<br/>"
    + "C'était un despote qui a gouvrné par la violence.</p>"
    + "<p>Mais c'est grâce à lui que notre système d'écriture, de mesure des poids et distances et "
    + "notre monnaie ont été standardisés dans toute la Chine.</p>";
// Hero
let event17Content = "<p>Ok, donc on a 7848 têtes et corps de soldats à rassembler par groupe de 12.</p>"
    + "<p>Si on arrive à résoudre 7848 : 12, on saura combien de groupes nous aurons</p>";
let event17Answer = "654";
// Hero
let event18Content = "<p>Et si on fait ça, le reste sera de combien ?</p>";
let event18Answer = "0";
// Hero
let event19Content = "<p>Je vois que nous avons 7164 chevaux, et chaque char prend 12 chevaux.</p>"
    + "<p>Combien devrions nous avoir de char ?</p>";
let event19Answer = "597";
// Hero
let event20Content = "<p>Regardez, maintenant qu'on a rassemblé les pièces des soldats, "
    + "un chemin à commencé à se libérer  jusqu'à Zhang Heng !</p>"
    + "<p>Des énigmes sont apparus sur les pavés...</p>";
// Hero
let event21Content = "<p>La première dit 'Vous trouverez la prochaine case à atteindre en trouvant "
    + "le reste de la division 3041 : 21'.</p>";
let event21Answer = "17";
// Hero
let event22Content = "<p>Ok, maintenant que nous avons avancé de 17 cases, nous avons une nouvelle "
    + "question :<br/>'La prochaine énigme à résoudre sera donnée par le quotient de 462 : 55'</p>";
let event22Answer = "8";
// Heng
let event23Content = "<p>Bravo !</p>"
    + "<p>Et merci pour tout les  efforts que vous avez fait pour venir me secourir ...</p>"
    + "<p>Les statues ont été détruites lors de mon arrivée ici, d'après ce que j'ai compris.</p>"
    + "<p>J'ai été traîné par une personne à l'allure étrange, qui me disait chercher les coordonnées "
    + "d'une ville ...</p>";
// Hero
let event24Content = "<p>Dites-nous tout, ces coordonnées nous permettrons de retrouver l'individu "
    + "qui est à l'origine de la situation actuelle !</p>";
// Heng
let event25Content = "<p>Oui, bien sûr, je vous dois bien ça.</p>"
    + "<p>Le premier calcul dont il était question, était 1578 : 35</p>";
let event25Answer = "45";
// Heng
let event26Content = "<p>Cela me semble juste ... Le second caclul dont il était question, c'était "
    + "le reste de la même opération, c'est-à-dire de 1578 : 35</p>";
let event26Answer = "3";
// Hero
let event27Content = "<p>Nous avons donc 45 et 3 pour coordonnées ... Ça m'a tout l'air d'être "
    + "quelque part en France, ça.</p>";
// Hero
let event28Content = "<p>À tout les coups il sera à Clermont Ferrand !</p>"
    + "<p>Cette ville faisait partie des villes que nous devions visiter !</p>";
// Heng
let event29Content = "<p>Bon, bah il ne me reste plus qu'à rentrer chez moi...</p>"
    + "<p>Bon courage dans votre mission !</p>";


export const CHAPTER4 : Chapter = new Chapter("Xi'an - Le grand mausolé.",
    [
        new Event("Xi'an", event1Content, null),
        new Event("Xi'an", event2Content, null),
        new Event("Xi'an", event3Content, null),
        new Event("Xi'an", event4Content, null),
        new Event("Xi'an", event5Content, NPCS['Zhang Heng']),
        new Event("Xi'an", event6Content, null),
        new Event("Xi'an", event7Content, NPCS['Zhang Heng']),
        new Event("Xi'an", event8Content, NPCS['Zhang Heng']),
        new Event("Xi'an", event9Content, NPCS['Zhang Heng'], [], [], event9Answer),
        new Event("Xi'an", event10Content, NPCS['Zhang Heng'], [], [], event10Answer),
        new Event("Xi'an", event11Content, NPCS['Zhang Heng'], [], [], event11Answer),
        new Event("Xi'an", event12Content, NPCS['Zhang Heng'], [], [], event12Answer),
        new Event("Xi'an", event13Content, null),
        new Event("Xi'an", event14Content, NPCS['Zhang Heng']),
        new Event("Xi'an", event15Content, null),
        new Event("Xi'an", event16Content, NPCS['Zhang Heng']),
        new Event("Xi'an", event17Content, null, [], [], event17Answer),
        new Event("Xi'an", event18Content, null, [], [], event18Answer),
        new Event("Xi'an", event19Content, null, [], [], event19Answer),
        new Event("Xi'an", event20Content, null),
        new Event("Xi'an", event21Content, null, [], [], event21Answer),
        new Event("Xi'an", event22Content, null, [], [], event22Answer),
        new Event("Xi'an", event23Content, NPCS['Zhang Heng']),
        new Event("Xi'an", event24Content, null),
        new Event("Xi'an", event25Content, NPCS['Zhang Heng'], [], [], event25Answer),
        new Event("Xi'an", event26Content, NPCS['Zhang Heng'], [], [], event26Answer),
        new Event("Xi'an", event27Content, null),
        new Event("Xi'an", event28Content, null),
        new Event("Xi'an", event29Content, NPCS['Zhang Heng']),
    ]
);
