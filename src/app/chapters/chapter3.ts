import { Chapter } from '../chapter';
import { Event } from '../event';
import { NPCS } from '../mock-heroes';

// Hero
let event1Content = "<p>Bon, nous sommes enfin arrivé Samos...</p>"
    + "<p>D'après les informations qu'on nous a fournit à l'entrée, "
    + "il y aurait une sorte de grande tour au centre de la ville...</p>";
// Hero
let event2Content = "<p>Je suppose qu'il était question de ce batiment ? "
    + "On dirait plutôt une sorte de gros coffre fort.</p>";
// Hero
let event3Content = "<p>Tu ne croyait pas si bien dire, je pense : la seule ouverture "
    + "est protégée par un verrou...</p>"
    + "<p>Voyons voir, un écritaux indique qu'il faut tourner le compteur de 258 centièmes "
    + "sur la droite, puis de 3 852 millièmes sur la gauche pour ouvrir...</p>"
// Pythagoras
let event4Content = "<p>Ohé ! En bas !</p>"
    + "<p>Ah, enfin, vous m'entendez... J'ai déjà essayé d'appeler les gens qui passent à "
    + "proximité à plusieurs reprises, mais aucun n'avait l'air de m'entendre ...</p>"
    + "<p>Bon, je peux vous donner des indices pour résoudre les problèmes qui vous permettrons "
    + "de venir me délivrer.</p>";
// Hero
let event5Content = "<p>Ah ! Mais ... Vous ne seriez pas Pythagore, par hasard ?</p>";
// Pythagoras
let event6Content = "<p>Ah ? Oui, c'est bien moi, mais comment avez-vous deviné ?</p>";
// Hero
let event7Content = "<p>Nous avons déjà été à Alexandrie, où nous avons retrouvé Euclide.<br/>"
    + "Vu que vous êtes originaire de Samos, on s'est douté que c'était vous !";
// Pythagoras
let event8Content = "<p>Ah, d'accord.</p>"
    + "<p>Mais dites-moi, on a pas l'air d'être à la même époque que la mienne<br/>"
    + "Comment se fait-il que vous me connaissiez aussi bien ?</p>";
// Hero
let event9Content = "<p>Il se trouve qu'on utilise toujours un théorème du même nom que vous<br/>"
    + "le 'Théorème de Pythagore'.</p>";
// Pythagore
let event10Content = "<p>Ah tiens, oui. Effectivement, j'ai fait des maths.</p>"
    + "<p>Mais à vrai dire, j'ai surtout marqué mon temps sur le plan religieux et philosophique."
    + "D'ailleurs, c'est moi qui ait créé le mot 'philosophe'.</p>"
    + "<p>Pour votre culture, mon nom signifie 'Celui qui a été annoncé par la Pythie'. "
    + "Et les phythies sont des sortes d'oracles qui vivent à Delphes, pas très loin d'ici.</p>";
// Pythagoras, explanation of rules
let event11Content = "<p>Pour en revenir à nos problèmes de fraction, une <b>fraction décimale</b> "
    + "s'écrit 1 / 10, 1 / 100, 1 / 1000. Elle permet d'écrire un nombre qui possède une "
    + "<b>partie décimale</b> c'est-à-dire une partie <b>après une virgule</b>.</p>"
    + "<p>Une fraction peut donc s'écrire sous la forme d'un nombre décimal.</p>"
    + "<p><img src=\"assets/img/resources/divisions1.jpg\" /></p>"
    + "<p>D'après cet exemple, on peut dire : 2 435 / 100 = 24,35.</p>";
// Hero first door: writing fractions in decimal form
let event12Content = "<p>Ok donc si on reprend ce qui est écrit sur la porte, il faut tourner "
    + "le compteur de 258 centièmes sur la droite, puis de 3 852 millièmes sur la gauche pour ouvrir...</p>"
    + "<p>Si on reprend votre explication, 258 centièmes s'écrit 258 / 100, et donc 2,58.</p>"
    + "<p>Du coup, comment s'écrit 3 852 millièmes ?</p>"
    + "<p>Attention à la virgule ...</p>";
let event12Answer = "3,852";
// Hero
let event13Content = "<p>Ok, ça a l'air de fonctionner, on va pouvoir avancer !</p>";
// Pythagoras
let event14Content = "<p>Attendez un instant, avant de vous lancer, vous aurez besoin de quelques informations "
    + "supplémentaires. Vous devez savoir que les nombres décimaux peuvent se <b>lire</b> de "
    + "<b>deux façons</b> différentes.</p>"
    + "<p>Prenons par exemple 24,35.</p>"
    + "<p>Il peut se lire vingt-quatre <b>unités </b>et trente-cinq <b>centièmes</b>.</p>"
    + "<p>Ou bien vingt-quatre <b>virgule</b> trente-cinq.</p>";
// Pythagoras
let event15Content = "<p>Histoire d'être sûr que ce soit clair, on va faire quelques tests.</p>"
    + "<p>Comment écrivez-vous 17 unités 5 dixièmes ?</p>";
let event15Answer = "17,5";
// Pythagoras
let event16Content = "<p>Et comment écrivez-vous 48 unités 2 dixièmes et 6 millièmes ?</p>"
    + "<p>Et n'oubliez pas, ici c'est comme pour les grands nombres : s'il y a un zéro, on ne donne pas le rang.</p>";
let event16Answer = "48,206";
// Pythagoras
let event17Content = "<p>Parfait, maintenant que vous avez compris cela, il ne nous reste qu'à voir comment "
    + "faire des opérations avec les nombres décimaux.</p>"
    + "<p>Pour <b>additionner</b> ou <b>soustraire</b> des nombres décimaux, vous <b>alignez</b> les chiffres par catégorie.<br/>"
    + "La séparation de la <b>partie entière</b> et de la <b>partie décimale</b> ne bouge pas : les <b>virgules</b> "
    + "doivent être <b>alignées</b>.<br/>"
    + "Si les <b>parties décimales</b> ne sont <b>pas identiques</b>, vous les <b>complétez</b> avec des <b>zéros</b>.</p>"
    + "<p>Dans le calcul, quand vous avez terminé avec la <b>partie décimale</b>, les <b>retenus</b> passent également dans "
    + "la <b>partie entière</b></p>"
    + "<p><img src=\"assets/img/resources/decimaux1.jpg\" /></p>";
// Hero
let event18Content = "<p>Ok, donc si on applique vos explications, on doit tourner la poignée de 2,58 sur la gauche, "
    + "puis 3,852 vers la droite. ça nous fait donc  3,852 - 2,58.</p>"
    + "<p>Et donc, ça fait combien ?</p>";
let event18Answer = "1,272";
// Pythagoras
let event19Content = "<p>Très bien, je vois que vous avez compris !</p>"
    + "<p>Je vous attends ici, à tout de suite !</p>";
// Hero second door
let event20Content = "<p>Nous voici à l'intérieur de la tour ... </p>"
    + "<p>Nous devons monter, mais une nouvelle porte nous en empêche.</p>"
    + "<p>Cette fois-ci, il est écrit 'Tourner de quatre-cent-quatre virgule huit, puis de cent-trente-trois unités "
    + "et cinq centièmes.'<br/>Ça m'a tout l'air d'être une <b>addition</b> de deux nombres décimaux !</p>"
    + "<p>Du coup, ça fait combien ?</p>";
let event20Answer = "537,85";
// Hero
let event21Content = "<p>C'est bon, la porte c'est ouverte, nous allons pouvoir monter !</p>"
    + "<p>Oula ! Il y a beaucoup de marches !!!</p>"
    + "<p>Je me demande combien il y en a .... Je vais les compter en montant.</p>"
    + "<p>1 ...</p><p>2 ...</p><p>3 ...</p><p>4 ...</p><p>5 ...</p>";
// Hero
let event22Content = "<p>C'est bon, va. On sait qu'il y en a beaucoup, ça nous suffira !</p>";
// Hero
let event23Content = "<p>284 marches, comme dans l'Arc de Triomphe, à Paris.</p>"
    + "<p>Enfin, nous voici arrivé devant la troisième porte de cette tour.</p>"
    + "<p>Cette fois-ci, il faut faire 125 virgule 96 vers la gauche, puis 24,09 vers la droite.</p>"
    + "<p>Ensuite, 82,47 vers la droite, et encore 7,53 vers la droite.</p>"
    + "<p>Je propose qu'on résolve déjà 125,96 - 24,09</p>";
let event23Answer = "101,87";
// Hero
let event24Content = "<p>Bien, maitnenant, on peut faire 101,87 + 82,47 + 7,53</p>";
let event24Answer = "191,87";
// Pythagoras Release Pythagoras
let event25Content = "<p>Ah ! Vous voilà !</p><p>Merci de m'avoir secouru !</p>"
    + "<p>J'aimerais bien rentrer à mon époque, maintenant...</p>"
    + "<p>J'ai observé la tour, depuis que je me suis retrouvé ici, et j'avais remarqué cette stèle, "
    + "au centre de la pièce. Il y est écrit 'La solution à votre problème se résoudra en additionnant "
    + "les résultats des deux premières portes, puis en y retranchant le résultat de la troisième porte.<br/>"
    + "Ensuite vous sera révélé un passage...'</p>";
// Hero
let event26Content = "<p>Ah, bah en reprenant les résultats que nous avons eu aux trois portes, ça fait "
    + "1,272 + 537,85 - 101,87</p>"
    + "<p>Et donc, quelle est la solution ?</p>";
let event26Answer = "437,252";
// Pythagoras
let event27Content = "<p>Regardez, un portail s'est révélé à la place de la fenêtre !</p>"
    + "<p>Il est écrit 'Soustrayez la <b>partie décimale</b> à la <b>partie entière</b>'</p>"
    + "<p>Eh bien, on en finit pas, avec les problèmes, ici...</p>";
let event27Answer = "185";
// Hero
let event28Content = "<p>Ça y est, le portail s'est ouvert !</p>"
    + "<p>Pythagore, je ne sais pas si vous aurez beaucoup de temps, mais pouvez vous juste nous dire"
    + "si vous avez vu quelque chose, en arrivant ici ?</p>";
// Pythagoras
let event29Content = "<p>Eh bien ... J'étais tranquillement en train d'enseigner mes étudiants sur "
    + "l'agora, quand j'ai reçu une grand coup dans le ventre, comme un coup de poing. J'ai vu des "
    + "étoiles de partout, puis une grande lumière blanche et j'ai cru que mon heure était venue...</p>"
    + "<p>Ensuite, je me suis retrouvé ici, et une voix m'a annoncé 'Vous resterez ici jusqu'à ce que "
    + "mes plans soient achevés. Il me faut juste votre savoir.'</p>"
    + "<p>Après je me suis retrouvé seul ici, mais j'étais souvent pris de fortes migraines, et j'avais "
    + "la sensation de m'affaiblir...</p>";
// Hero
let event30Content = "<p>On dirait que quelqu'un essayait de vous soutirer vos connaissances ...</p>"
    + "<p>Mais comment s'y prenait-il ?</p>"
    + "<p>Enfin, ne tardez pas, le portail s'est mis à faire un bruit différend. Si ça se trouve, "
    + "il va bientôt se refermer.</p>";
// Pythagoras
let event31Content = "<p>Très bien ... Au revoir, et merci pour tout !</p>";
// Hero
let event32Content = "<p>Je ne sais pas ce qu'il se passe, mais clairement, quelqu'un d'hostile "
    + "se cache derrière ces évènements !</p>";

export const CHAPTER3 : Chapter = new Chapter("Samos - Le coffre aux dix millièmes.",
    [
        new Event("Samos", event1Content, null),
        new Event("Samos", event2Content, null),
        new Event("Samos", event3Content, null),
        new Event("Samos", event4Content, NPCS['Pythagoras']),
        new Event("Samos", event5Content, null),
        new Event("Samos", event6Content, NPCS['Pythagoras']),
        new Event("Samos", event7Content, null),
        new Event("Samos", event8Content, NPCS['Pythagoras']),
        new Event("Samos", event9Content, null),
        new Event("Samos", event10Content, NPCS['Pythagoras']),
        new Event("Samos", event11Content, NPCS['Pythagoras']),
        new Event("Samos", event12Content, null, [], [], event12Answer),
        new Event("Samos", event13Content, null),
        new Event("Samos", event14Content, NPCS['Pythagoras']),
        new Event("Samos", event15Content, NPCS['Pythagoras'], [], [], event15Answer),
        new Event("Samos", event16Content, NPCS['Pythagoras'], [], [], event16Answer),
        new Event("Samos", event17Content, NPCS['Pythagoras']),
        new Event("Samos", event18Content, null, [], [], event18Answer),
        new Event("Samos", event19Content, NPCS['Pythagoras']),
        new Event("Samos", event20Content, null, [], [], event20Answer),
        new Event("Samos", event21Content, null),
        new Event("Samos", event22Content, null),
        new Event("Samos", event23Content, null, [], [], event23Answer),
        new Event("Samos", event24Content, null, [], [], event24Answer),
        new Event("Samos", event25Content, NPCS['Pythagoras']),
        new Event("Samos", event26Content, null, [], [], event26Answer),
        new Event("Samos", event27Content, NPCS['Pythagoras'], [], [], event27Answer),
        new Event("Samos", event28Content, null),
        new Event("Samos", event29Content, NPCS['Pythagoras']),
        new Event("Samos", event30Content, null),
        new Event("Samos", event31Content, NPCS['Pythagoras']),
        new Event("Samos", event32Content, null),
    ]
);
