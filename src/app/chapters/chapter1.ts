import { Chapter } from '../chapter';
import { Event } from '../event';
import { NPCS } from '../mock-heroes';

// Boole
let event1Content = "<p>Bienvenue à Londres !<br/>"
    + "Je me présente, je suis George Boole. Je suis mathématicien et philosophe, né en 1815 en Angleterre.<br/>"
    + "Je suis l'auteur de l'algèbre binaire, qui anime tout les ordinateurs (enfin, ça, je ne suis pas sensé "
    + "le savoir, les ordinateurs n'existent pas encore à notre époque).</p>";
// Cauchy
let event2Content = "<p>Bonjour jeunes gens, je m'appelle Augustin-Louis Cauchy. "
    + "Je suis un mathématicien Français.<br/>"
    + "Je suis notamment réputé comme étant un auteur prolifique. Mais il est vrai qu'on m'attribut près de 800 articles"
    + " dans des journaux scientifiques et 7 ouvrages sur les mathématiques.</p>"
    + "<p>Néanmoins, Les travaux dont je suis le plus fier portent sur les suites et séries numériques.</p>";
// Babbage
let event3Content = "<p>Bonjour, nous avons fait appel à vous pour résoudre un grave problème.</p>";
// Cauchy
let event4Content = "<p>Charles, n'oublie pas de te présenter, quand même.<br/>"
    + "Tu va faire fuir nos hôtes, sinon.</p>";
// Babbage
let event5Content = "<p>Hum, oui, tu as raison, Augustin.<br/>"
    + "Je m'appelle Charles Babbage. Je suis mathématicien et inventeur.</p>"
    + "<p>C'est moi qui ait inventé le concept de l'ordinateur avec mes machines à calculer. Je suis en train "
    + "de travailler sur une machine qui sera capable d'exécuter des programmes prédéfinis pour effectuer des calculs "
    + "mathématiques complexes en un rien de temps !</p>"
    + "<p>Comment avez vous dit ? Une calculette ? Qui tiens dans une poche ? Voyons, c'est impossible, ma machine rentre "
    + "à peine dans un hangar !</p>";
// Boole
let event6Content = "<p>Tu peux parler, Augustin. À accueillir nos invités avec un langage aussi ampoulé, "
    + "toi aussi, tu risques de les faire fuir!</p>";
// Cauchy
let event7Content = "<p>Oui, bon, ok... Nous devrions en revenir à notre problème, maintenant.</p>";
// Hero 1
let event8Content = "<p>Dans votre courrier, vous faisiez allusion à des ... Distortions ?</p>";
// Boole
let event9Content = "<p>Oui, je comprend que cela vous paraisse étrange. Nous même avons eu du mal à trouver un terme.<br/>"
    + "En fait, en plusieurs endroit du globe, nous avons observé d'étranges phénomènes, avec des lieux du passé "
    + "qui apparaissent en plein mileu des villes. Comme si le passé se mélangeait au présent.</p>";
// Babbage
let event10Content = "<p>Oui, nous avons fait appel à vous pour enquêter sur ces évènements.<br/>"
    + "Vous allez devoir vous rendre sur chacun des sites et essayer d'en déterminer l'origine !<br/>"
    + "Mais pour cela, vous allez déjà devoir les localiser, et nous avons besoin de vos connaissances sur les grands nombres...</p>";
// Cauchy
let event11Cotnent = "<p>Oui, enfin, nous devrions d'abord leurs donner quelques outils pour les aider...<br/>"
    + "Voici déjà quelques rappelles sur le système décimal :</p>"
    + "<ul>"
    + "<li>Notre système de numération se compose de <b>classes de nombres</b> : les milliards, les millions, les milliers, et les unités.</li>"
    + "<li>Chaque classe comprend <b>3 catégories de chiffres</b> : les centaines, les dizaines, et les unités</li>"
    + "<li>Quand vous écrivez ou lisez un nombre, vous écrivez ou prononcez les <b>chiffres</b>, "
    + "puis le <b>nom de la classe</b>, et vous laissez un <b>espace</b></li>"
    + "</ul>"
    + "<p>Voici un exemple : 13 252 454 se lit <b>13 millions 252 mille 454</b>.</p>";
// Boole
let event12Content = "<p>Pour vous aider à représenter les grands nombres, vous pouvez utiliser ce qu'on appelle un tableau de numération.<br/>"
    + "Voici un exemple :</p>"
    + "<table>"
    + "<tr>"
    + "<th colspan=\"3\"><b>Classe des milliards</b></th>"
    + "<th colspan=\"3\"><b>Classe des millions</b></th>"
    + "<th colspan=\"3\"><b>Classe des milliers</b></th>"
    + "<th colspan=\"3\"><b>Classe des unités</b></th>"
    + "</tr>"
    + "<tr>"
    + "<th>Cent</th><th>Dix</th><th>Unités</th>"
    + "<th>Cent</th><th>Dix</th><th>Unités</th>"
    + "<th>Cent</th><th>Dix</th><th>Unités</th>"
    + "<th>Cent</th><th>Dix</th><th>Unités</th>"
    + "</tr>"
    + "<tr>"
    + "<td></td><td></td><td>2</td>"
    + "<td>6</td><td>0</td><td>8</td>"
    + "<td>8</td><td>7</td><td>5</td>"
    + "<td>2</td><td>0</td><td>5</td>"
    + "</tr>"
    + "<tr>"
    + "<td></td><td></td><td></td>"
    + "<td></td><td>3</td><td>2</td>"
    + "<td>0</td><td>5</td><td>8</td>"
    + "<td>0</td><td>1</td><td>6</td>"
    + "</tr>"
    + "</table>"
    + "<p>Ici, nous avons :</p>"
    + "<ul>"
    + "<li>deux-<b>milliards</b> six-cent-huit-<b>millions</b> huit-cent-soixante-quinze-<b>mille</b> deux-cent-cinq</li>"
    + "<li>trente-deux-<b>millions</b>-cinquante-huit-<b>mille</b> seize.</li>"
    + "</ul>"
    + "<p>Tu notera que milliard et million s'<b>accordent</b> : ils prennent un 's' au pluriel.</p>";
// Babbage
let event13Content = "<p>Bon, tu leur en a assez dit, Augustin. Maintenant, on va voir s'ils ont compris de quoi on parle...</p>"
    + "<p>Écrivez-moi Vingt-cinq-millions huit-cent-soixante-quinze-mille quatre-cent-trente-neuf en chiffre.</p>"
    + "<p>Bon, et si vous avez du mal, n'hésitez pas à vous entre aider, et écrire sur une feuille en séparant bien les classes de nombre...</p>";
let event13Answer = "25 875 439";
// Boole
let event14Content = "<p>Charles, tu fait genre aggresif, mais en fait, tu commences gentils, avec nos invités !</p>"
    + "<p>Les enfants, essayez donc de m'écrire cinq-cent-trente-sept-milliards deux-cent-quatre-vingt-seize-millions cent-soixante-sept-mille deux...</p>"
    + "<p>... Bon, et pour rappel, si une catégorie contient un zéro, elle n'est pas écrite.</p>";
let event14Answer = "537 296 167 002";
// Cauchy
let event15Content = "<p>Très bien, les enfants, maintenant, voyons si vous savez faire la même chose dans l'autre sens...</p>"
    + "<p>Pouvez-vous m'écrire 100 001 010 en lettres?</p>"
    + "<p>Et n'oubliez pas les tirets...</p>";
let event15Answer = "cent-millions mille dix";
// Cauchy
let event16Content = "<p>Très bien ! Messieurs, je penses que vous êtes d'accord avec moi pour dire que ces jeunes gens sont fin prêt ?</p>";
// Boole
let event17Content = "<p>Pas tout à fait, Augustin. J'aimerais que nous leur parlions encore de la décomposition des grands nombres...</p>";
// Babbage
let event18Content = "<p>Ha George ! Tu perds ton temps, ils sauront faire sans...</p>";
// Boole
let event19Content = "<p>Je penses que ça ne fera pas de mal de revoir cette méthode, je penses ... Vous êtes d'accord avec moi ?</p>";
// Hero 2
let event20Content = "<p>Je suppose que ça ne coûte rien ?</p>";
// Boole
let event21Content = "<p>Bien, alors vous pouvez <b>décomposer</b> un grand nombre par <b>classe</b> ou par <b>catégorie</b></p>"
    + "<p>Par exemple : 7 340 268 = (7 X 1 000 000) + (340 X 1000) + (268 X 1)<br/>"
    + "Ou bien (7 X 1 000 000) + (3 X 100 000) + (4 X 10 000) + (2 X 100) + (6 X 10) + (8 X 1)</p>"
    + "<p>Chaque chiffre représente une <b>catégorie</b> et a une <b>valeur</b>.</p>"
    + "<p>Chaque <b>catégorie</b> représente une <b>quantité</b> dans le nombre.<br/>"
    + "Par exemple, dans 7 3<b>4</b>0 268, le <b>4</b> est le <b>chiffre</b> des dizaines de mille.<br/>"
    + "Toujours dans 7 <b>34</b>0 268, 34 est le <b>nombre</b> de dizaines de mille.</p>";
// Babbage
let event22Content = "<p>Bien, j'espère que vous avez compris, parce que c'est le moment de passer à la casserole !</p>"
    + "<p>À quel nombre (en chiffre) correspond (4 X 1 000 000) + (6 X 10 000) + (8 X 100) + (5 X 10) ?</p>";
let event22Answer = "4 060 850";
// Cauchy
let event23Content = "<p>Très bien.</p><p>Maintenant, essayez donc de retrouver le nombre (toujours en chiffre) pour "
    + "(57 X  10 000 000) + (683 X 1 000) + (21 X 10).</p>";
let event23Answer = "570 683 210";
// Boole
let event24Content = "<p>Bien bien bien ... Maintenant, nous allons voir comment ranger des nombres dans l'ordre.</p>";
// Babbage
let event25Content = "<p>Beaucoup trop simple, voyons !</p>"
    + "<p>Pour <b>ordonner</b> et <b>ranger</b> des nombres, il suffit de les <b>comparer</b>. Pour cela, vous devez toujours "
    + "commencer par compter le nombre total de leurs chiffres. Le plus grand sera celui qui va le plus loin dans les classes...<br/>"
    + "Par exemple, 1 100 101 111 &gt; 9 457 999 car 1 100 101 111 a dix chiffres, alors que 9 457 999 n'en a que 7...</p>"
    + "<p>Pourquoi prenez vous cet air ahuri ? Ah, s'ils ont le même nombre de chiffre ? ... Oui, évidement, ça ne suffit pas.</p>"
    + "<p>Dans ce cas là, rien de plus simple : vous comparerez <b>catégorie par catérorie</b> en partant de la plus grande.</p>"
    + "<p>Par exemple, si vous avez 1 100 1<b>0</b>1 111 et 1 100 1<b>3</b>1 111, 1 100 1<b>0</b>1 111 &lt; 1 100 1<b>3</b>1 111, "
    + "car sur la dizaine de milliers, <b>0 &lt; 3</b>.";
// Cauchy
let event26Content = "<p>Très bien, Charles, maintenant voyons voir si l'explication était claire...</p>"
    + "<p>Pouvez-vous me classer les lettres suivantes dans l'ordre croissant de la valeur qui leur est associée :</p>"
    + "<ul>"
    + "<li>A: 50 007 341 234</li>"
    + "<li>B: 735 067 328</li>"
    + "<li>C: 4 320 744 012</li>"
    + "<li>D: 735 077 832</li>"
    + "<li>E: 50 007 321 534</li>"
    + "</ul>"
    + "<p>Pour rappel, l'<b>ordre croissant</b>, ça signifie du plus petit au plus grand.</p>";
let event26Answer = "B, D, C, E, A";
// Babbage
let event27Content = "<p>Tout cela était trop simple, et si nous mélangions le tout, maintenant ?</p>"
    + "<p>Dites-moi, est-ce que sept-millions trente-deux-mille huit est plus grand que 7 031 009 ?</p>"
    + "<p>Répondez par 'oui' si vous pensez que c'est vrai, sinon par 'non'.</p>";
let event27Answer = "oui";
// Babbage
let event28Content = "<p>Ok, ok... Encore trop simple, c'est ça ?</p>"
    + "<p>Est-ce que (6 X 1 000 000) + (5 X 100 000) + (4 X 10) + (9 X 1) est plus petit que 6 400 031 ?</p>"
    + "<p>Répondez par 'oui' si vous pensez que c'est vrai, sinon par 'non'.</p>"
let event28Answer = "non";
// Boole
let event29Content = "<p>Bon, je penses qu'ils sont paré pour la suite, maintenant ...</p>"
    + "<p>Qu'en pensez-vous, messieurs ?</p>";
// Cauchy
let event30Content = "<p>C'est tout bon, pour moi</p>";
// Babbage
let event31Content = "<p>Mouais, ça devrait le faire ...</p>";
// Boole
let event32Content = "<p>Bien, alors dans ce cas, voici la liste des cités dans lesquelles des distortions nous ont été rapportées...</p>"
    + "<p>La première a été observée à Alexandria, en Égypte. Le même phénomène a également été signalé à Samos, en Grèce, "
    + "et à Clermont-Ferrand, en France.</p>";
// Cauchy
// TODO check Leipzig location
let event33Content = "<p>Pendant que nous discutions, nous avons reçu de nouveaux rapports.</p>"
    + "<p>Le même phénomène s'est également produit à Nanyang, au find fond de la Chine, et à Leipzig, en Russie</p>"
    + "<p>Et ce n'est pas tout ! Un nouveau rapport nous est parvenu de Clermont Ferrand... Des gens ont pu observer à l'intérieur de la distorsion. "
    + "Il disent y avoir vu comme une version plus ancienne de la ville, avec un homme retenu à l'intérieur... Un observateur dit avoir reconnu "
    + "Blaise Pascal...</p>";
    + "<p>Le même phénomène s'est également produit à Nanyang, au find fond de la Chine, et à Leipzig, en Russie</p>"
    + "<p>Et ce n'est pas tout !<br/>Un nouveau rapport nous est parvenu de Clermont Ferrand... Des gens ont pu observer à l'intérieur de la distortion. "
    + "Il disent y avoir vu comme une version plus ancienne de la ville, avec un homme retenu à l'intérieur... Un observateur dit avoir reconnu "
    + "Blaise Pascal...</p>";
// Boole
let event34Content = "<p>Vous voulez dire... Le scientifique ? Mais c'est impossible, il est mort depuis un moment...</p>";
// Hero 3
let event35Content = "<p>Ne vous inquiétez pas, mes compagnons et moi-même, nous nous chargerons d'élucider ces mystères...</p>";
// Hero 1
let event36Content = "<p>Les gens, il ne nous reste plus qu'à choisir notre première destination !</p>";

export const CHAPTER1 : Chapter = new Chapter("Les grands nombres",
    [
        new Event("Introduction", event1Content, NPCS['George Boole']),
        new Event("Introduction", event2Content, NPCS['Augustin-Louis Cauchy']),
        new Event("Introduction", event3Content, NPCS['Charles Babbage']),
        new Event("Introduction", event4Content, NPCS['Augustin-Louis Cauchy']),
        new Event("Introduction", event5Content, NPCS['Charles Babbage']),
        new Event("Introduction", event6Content, NPCS['George Boole']),
        new Event("Introduction", event7Content, NPCS['Augustin-Louis Cauchy']),
        new Event("Introduction", event8Content, null),
        new Event("Introduction", event9Content, NPCS['George Boole']),
        new Event("Introduction", event10Content, NPCS['Charles Babbage']),
        new Event("Introduction", event11Cotnent, NPCS['Augustin-Louis Cauchy']),
        new Event("Introduction", event12Content, NPCS['George Boole']),
        new Event("Introduction", event13Content, NPCS['Charles Babbage'], [], [], event13Answer),
        new Event("Introduction", event14Content, NPCS['George Boole'], [], [], event14Answer),
        new Event("Introduction", event15Content, NPCS['Augustin-Louis Cauchy'], [], [], event15Answer),
        new Event("Introduction", event16Content, NPCS['Augustin-Louis Cauchy']),
        new Event("Introduction", event17Content, NPCS['George Boole']),
        new Event("Introduction", event18Content, NPCS['Charles Babbage']),
        new Event("Introduction", event19Content, NPCS['George Boole']),
        new Event("Introduction", event20Content, null),
        new Event("Introduction", event21Content, NPCS['George Boole']),
        new Event("Introduction", event22Content, NPCS['Charles Babbage'], [], [], event22Answer),
        new Event("Introduction", event23Content, NPCS['Augustin-Louis Cauchy'], [], [], event23Answer),
        new Event("Introduction", event24Content, NPCS['George Boole']),
        new Event("Introduction", event25Content, NPCS['Charles Babbage']),
        new Event("Introduction", event26Content, NPCS['Augustin-Louis Cauchy'], [], [], event26Answer),
        new Event("Introduction", event27Content, NPCS['Charles Babbage'], [], [], event27Answer),
        new Event("Introduction", event28Content, NPCS['Charles Babbage'], [], [], event28Answer),
        new Event("Introduction", event29Content, NPCS['George Boole']),
        new Event("Introduction", event30Content, NPCS['Augustin-Louis Cauchy']),
        new Event("Introduction", event31Content, NPCS['Charles Babbage']),
        new Event("Introduction", event32Content, NPCS['George Boole']),
        new Event("Introduction", event33Content, NPCS['Augustin-Louis Cauchy']),
        new Event("Introduction", event34Content, NPCS['George Boole']),
        new Event("Introduction", event35Content, null),
        new Event("Introduction", event36Content, null),
    ]
);
