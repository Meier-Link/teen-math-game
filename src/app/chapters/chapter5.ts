import { Chapter } from '../chapter';
import { Event } from '../event';
import { NPCS } from '../mock-heroes';

// Hero
let event1Content = "<p>Nous voici à Clermont-Ferrand ... Mais il fait bien sombre ...</p>";
let event2Content = "<p>Regarde au ciel, et tu comprendra : il y a une sorte d'énorme château "
    + "qui flotte au dessus de la ville</p>";
let event3Content = "<p>Cela explique l'agitation dans la ville ...</p>"
    + "<p>Je vois qu'une sorte de portail permet d'y accéder ... Il va bien falloir nous lancer !</p>";
// Pascal
let event4Content = "<p>Ohé les jeunes !</p><p>N'avancez pas plus, c'est dangereux, là dedans !</p>"
    + "<p>Cette bâtisse est occupée par une espèce de folle qui fait des expériences étranges ...</p>"
    + "<p>Je suis Blaise Pascal. Je ne sais pas ce qu'elle veut faire de moi, mais elle me retient ici ...</p>";
// Hero
let event5Content = "<p>Nous sommes là pour l'arrêter</p><p>Pouvez-vous nous dire à quoi elle joue ?</p>";
// Pascal
let event6Content = "<p>Je ne penses pas que ce soit un jeu, on dirait bien qu'elle veut conquérir le monde ...</p>"
    + "<p>Pour que vous soyez équipés, je dois vous expliquer rapidement comment manipuler les différentes mesures.</p>"
    + "<p>Déjà, rappelez-vous que les <b>unités</b> de <b>longueur</b>, de <b>masse</b> et de <b>contenance</b> "
    + "font partie du <b>système décimal</b></p>";
let event6bContent = "<p>Le tableau suivant indique leur unité principale :</p>"
    + "<p><img src=\"assets/img/resources/poids-mesures1.jpg\" /></p>"
    + "<p>Je vous conseil de garder ce tableau sous le coude pour convertir les poids et mesures ...</p>";
let event7Content = "<p>Les <b>sous-multiples</b> représentent une <b>division</b> de l'unité par 10, 100, 1 000 ...</p>"
    + "<p>les <b>multiples</b> représentent une <b>multiplication</b> de l'unité par 10, 100, 1 000 ...</p>"
    + "<p>À titre d'exemple, dites moi qu'elle unité de mesure vous utiliseriez pour vous peser ? le 'kg' ? le 'g' ? le 'mg'</p>";
let event7Answer = "kg";
let event8Content = "<p>Ok pour la mesure du poids.</p>"
    + "<p>et pour mesure une piste de décollage, vous utiliseriez le 'km', le 'mm' ou le 'cm' ?";
let event8Answer = "km";
let event9Content = "<p>Ok pour les distances.</p>"
    + "<p>Et selon vous, un verre aura une contenance de plutôt 25dL, 25cL ou 25mL";
let event9Answer = "25cl";
// Hero
let event10Content = "<p>Merci pour ces informations ! On arrive !</p>";
// Laevatein
let event11Content = "<p>Pas si vite, les jeunes !</p>"
    + "<p>Je ne vais pas vous laisser mettre des bâtons dans mes plans comme ça !</p>";
// Hero
let event12Content = "<p>Qui êtes vous ? Que faites vous ici ?</p>";
// Laevatein
let event13Content = "<p>Qui je suis ? Je suis Laevatein. Quant à ici, eh bien je suis chez moi</p>";
// Hero
let event14Content = "<p>Vous vous déplacez avec votre maison, vous ?</p>";
// Laevatein
let event15Content = "<p>Héhéhé ... Disons que c'est plus pratique ...</p>"
    + "<p>Mais trève de bavardages. Il ne me manque pas grand chose pour pouvoir mettre mon plan à exécution...</p>"
    + "<p>Pour cela, j'ai besoin de 725 cL d'acide chloridryque et 10,5 L d'acétone ... De quelle capacité devra "
    + "être mon réservoir, en litre ?";
let event15Answer = "17,75";
// Laevatein
let event16Content = "<p>Ah ! Comment avez-vous osé me voler la réponse ?</p>"
    + "<p>Ok, dans ce cas, tentez donc de deviner la masse totale d'explosif nécessaire pour faire sauter cet engin "
    + "sachant qu'il faut 940 kg de nitrate d'ammonium, 17,625 q de TNT et 58 750 g de nitro glycérine ?";
// Hero
let event17Content = "<p>Ça va être dur de trouver le résultat en une seule fois. Nous devrions déjà mettre toute "
    + "les mesures sur la même unité ...</p>"
    + "<p>Comment écrire 17,625 q en kg ?</p>"
    + "<p>Avec le tableau que nous a montré Blaise Pascal, on devrait s'en sortir assez facilement.</p>";
let event17Answer = "176,25";
let event18Content = "<p>Ok, on commence à y voir un peu plus clair ...</p>"
    + "<p>Maintenant, 58 750 g en kg ?</p>";
let event18Answer = "58,75";
let event19Content = "<p>Parfait !</p>"
    + "<p>Il ne nous reste plus qu'à faire 940 + 176,25 + 58,75</p>";
let event19Answer = "1175";
// Laevatein-Injured
let event20Content = "<p>Vous m'avez encore doublé !</p>"
    + "<p>Il ne me reste plus qu'à me retirer dans mon époque et recommencer ... </p>";
// Hero
let event21Content = "<p>Nous ne vous laisserons pas repartir ... </p>";
// Laevatein-Injured
let event22Content = "<p>Essayez donc !</p>"
    + "<p>Pour partir, j'ai juste besoin de 25,8 tonnes de di-hydrogène et 132 720 kg de di-oxygène liquide.</p>"
    + "<p>Il ne me reste plus qu'à trouver un réservoir assez grand pour supporter la somme de leurs masses ...</p>";
// Hero
let event23Content = "<p>Pour trouver, il faut déjà convertir 25,8 tonnes en kg.</p>";
let event23Answer = "25800";
let event24Content = "<p>Bien, il ne nous reste plus qu'à aire 132 720 + 25 800</p>";
let event24Answer = "158520";
// Laevatein-Injured
let event25Content = "<p>Sérieusement ? Vous m'avez encore doublé ???</p>"
    + "<p>Rhaaaa ! Tout mes plans sont à l'eau !</p>";
// Hero
let event26Content = "<p>Nous y sommes arrivés ! Il ne reste plus qu'à libérer Blaise Pascal et remettre Laevatein "
    + "dans un cachot !</p>";
// Pascal
let event27Content = "<p>Bravo, et merci pour tout ce que vous avez fait !</p>";


export const CHAPTER5 : Chapter = new Chapter("Clermont Ferrand - La conquérante.",
    [
        new Event("Clermont Ferrand", event1Content, null),
        new Event("Clermont Ferrand", event2Content, null),
        new Event("Clermont Ferrand", event3Content, null),
        new Event("Clermont Ferrand", event4Content, NPCS['Blaise Pascal']),
        new Event("Clermont Ferrand", event5Content, null),
        new Event("Clermont Ferrand", event6Content, NPCS['Blaise Pascal']),
        new Event("Clermont Ferrand", event6bContent, NPCS['Blaise Pascal']),
        new Event("Clermont Ferrand", event7Content, NPCS['Blaise Pascal'], [], [], event7Answer),
        new Event("Clermont Ferrand", event8Content, NPCS['Blaise Pascal'], [], [], event8Answer),
        new Event("Clermont Ferrand", event9Content, NPCS['Blaise Pascal'], [], [], event9Answer),
        new Event("Clermont Ferrand", event10Content, null),
        new Event("Clermont Ferrand", event11Content, NPCS['Laevatein']),
        new Event("Clermont Ferrand", event12Content, null),
        new Event("Clermont Ferrand", event13Content, NPCS['Laevatein']),
        new Event("Clermont Ferrand", event14Content, null),
        new Event("Clermont Ferrand", event15Content, NPCS['Laevatein'], [], [], event15Answer),
        new Event("Clermont Ferrand", event16Content, NPCS['Laevatein']),
        new Event("Clermont Ferrand", event17Content, null, [], [], event17Answer),
        new Event("Clermont Ferrand", event18Content, null, [], [], event18Answer),
        new Event("Clermont Ferrand", event19Content, null, [], [], event19Answer),
        new Event("Clermont Ferrand", event20Content, NPCS['Laevatein-Injured']),
        new Event("Clermont Ferrand", event21Content, null),
        new Event("Clermont Ferrand", event22Content, NPCS['Laevatein-Injured']),
        new Event("Clermont Ferrand", event23Content, null, [], [], event23Answer),
        new Event("Clermont Ferrand", event24Content, null, [], [], event24Answer),
        new Event("Clermont Ferrand", event25Content, NPCS['Laevatein-Injured']),
        new Event("Clermont Ferrand", event26Content, null),
        new Event("Clermont Ferrand", event27Content, NPCS['Blaise Pascal']),
    ]
);
