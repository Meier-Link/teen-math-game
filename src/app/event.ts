import { Hero } from './hero';

export class Event {
    actor: Hero;
    title: string;
    content: string;
    choices: string[];
    answerStr: string;
    answerNums: number[];

    constructor(title: string, content: string, actor: Hero = null,
        choices: string[] = [], answerNums: number[] = [],
        answerStr: string = null) {
        // If choices is provided, answerNum contains the answer
        // Otherwise, answerStr is the answer
        this.title = title;
        this.content = content;
        this.actor = actor;
        this.choices = choices;
        this.answerStr = answerStr;
        this.answerNums = answerNums;
    }

    hasChoices() : boolean {
        if (this.choices.length > 0 && this.answerNums.length > 0)
            return true;
        return false;
    }

    hasAnswerField() : boolean {
        if (this.answerStr != null)
            return true;
        return false;
    }

    // Case of input type text.
    isValidStrAnswer(playerAnswer: String) : boolean {
        playerAnswer = playerAnswer.toLowerCase().replace(/ /g, '');
        let answerStr = this.answerStr.toLowerCase().replace(/ /g, '');
        console.log(playerAnswer + " --- " + answerStr);
        if (playerAnswer == answerStr)
            return true;
        return false;
    }

    // Case of multi choices
    isValidNumAnswers(playerAnswers: number[]) : boolean {
        if (playerAnswers.length != this.answerNums.length)
            return false;

        let reauiredAnswers = this.answerNums.sort();
        playerAnswers = playerAnswers.sort();

        for (let i = 0; i < reauiredAnswers.length; i++) {
            if (reauiredAnswers[i] != playerAnswers[i])
                return false;
        }

        return true;
    }
}
