import { Component, OnInit } from '@angular/core';
//import { Level } from '../level';
import { PLAYERS } from '../mock-players';
import { Chapter } from '../chapter';
import { Event } from '../event';

import { CHAPTER1 } from '../chapters/chapter1';
import { CHAPTER2 } from '../chapters/chapter2';
import { CHAPTER3 } from '../chapters/chapter3';
import { CHAPTER4 } from '../chapters/chapter4';
import { CHAPTER5 } from '../chapters/chapter5';

@Component({
  selector: 'app-game-engine',
  templateUrl: './game-engine.component.html',
  styleUrls: ['./game-engine.component.css']
})
export class GameEngineComponent implements OnInit {
    players = PLAYERS;
    chapters = [CHAPTER1, CHAPTER2, CHAPTER3, CHAPTER4, CHAPTER5];
    currentChapter = null;
    currentEvent = null;
    answerField: string;
    answerNums: number[];
    errorEvent: Event;
    currentHero: number;

    constructor() {
        console.log(this.players);
        this.answerField = "";
        this.answerNums = [];
        this.currentHero = 0;
    }

    curentHeroFull() : string {
        // If we hit the beginning or the end of the list, go to the opposite.
        if (this.currentHero > (this.players.length - 1)) this.currentHero = 0;
        if (this.currentHero < 0) this.currentHero = this.players.length - 1;

        return this.players[this.currentHero].character.full();
    }

    ngOnInit() {
    }

    onClickChapter(chapter: Chapter) {
        this.currentChapter = chapter;
        this.currentEvent = chapter.getCurrentEvent();
    }

    onClickPrev() {
        console.log(this.currentChapter.progress);
        if (this.currentChapter.progress > 0) {
            this.currentChapter.progress--;
            this.currentEvent = this.currentChapter.getCurrentEvent();
            if (this.currentEvent.actor == null) {
                this.currentHero--;
            }
        } else {
            this.currentEvent = null;
            this.currentChapter = null;
        }
    }

    onClickNext() {
        // Do check if event conditions is filled
        let errors = false;
        if (this.currentEvent.hasChoices()) {
            // TODO Check choices
        }
        if (this.currentEvent.hasAnswerField()) {
            if (! this.currentEvent.isValidStrAnswer(this.answerField)) {
                this.currentChapter.progress--;
                errors = true;
                this.errorEvent = new Event(
                    this.currentEvent.title,
                    "Ce n'était pas la bonne réponse !<br/>Mais tu vas avoir une nouvelle chance.",
                    this.currentEvent.actor);
                this.currentEvent = this.errorEvent;
            }
        }
        // If no error, go to the next step.
        if (! errors) {
            this.answerField = "";
            this.answerNums = [];
            if (! this.currentChapter.hitEndOfChapter()) {
                this.currentChapter.progress++;
                this.currentEvent = this.currentChapter.getCurrentEvent();
                if (this.currentEvent.actor == null) {
                    this.currentHero++;
                }
            } else {
                this.currentEvent = null;
                this.currentChapter = null;
            }
        }
    }
}
