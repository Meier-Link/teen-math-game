export class Entity {
  	name: string;
    x: number;
    y: number;
    /* Provided in the constructor to associate click events. */
    onClick: Function;
}
