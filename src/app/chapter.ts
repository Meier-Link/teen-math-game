import { Event } from './event';

export class Chapter {
    events: Event[];
    title: string;
    progress: number;

    constructor(title: string, events: Event[]) {
        this.title = title;
        this.events = events;
        this.progress = 0;
    }

    addEvent(newEvent: Event) {
        this.events.push(newEvent);
    }

    getCurrentEvent() : Event {
        return this.events[this.progress];
    }

    hitEndOfChapter() : boolean {
        //if (this.events.length < (this.progress + 1))
        if ((this.progress + 1) >= this.events.length)
            return true;
        return false;
    }
}
