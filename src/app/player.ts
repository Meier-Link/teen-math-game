import { Hero } from './hero';

export class Player {
    firstname: string;
    lastname: string;
    age: number;
    classroom: string;
    character: Hero;

    constructor() {
        this.firstname = "";
        this.lastname = "";
        this.age = 0;
        this.classroom = "";
        this.character = null;
    }
}
