import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HeroesComponent } from './heroes/heroes.component';
import { PlayersComponent } from './players/players.component';
import { GameEngineComponent } from './game-engine/game-engine.component';


@NgModule({
    declarations: [
        AppComponent,
        HeroesComponent,
        PlayersComponent,
        GameEngineComponent
    ],
    imports: [
        BrowserModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
