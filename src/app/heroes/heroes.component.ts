import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';

@Component({
    selector: 'app-heroes',
    templateUrl: './heroes.component.html',
    styleUrls: ['./heroes.component.css']
})

export class HeroesComponent implements OnInit {
    hero: Hero = new Hero('Ike', 'warrior', 39, 35);

    heroes: Hero[] = [
        new Hero('Ike', 'warrior', 39, 35),
        new Hero('Cecilia', 'warrior', 33, 39),
        new Hero('Daraen', 'alchemist', 30, 31),
        new Hero('Tiki', 'alchemist', 32, 29)
    ];

    constructor() { }

    ngOnInit() {
    }

}
