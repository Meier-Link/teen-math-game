import { Player } from './player';

export class Hero {
  	name: string;
    job: string;
    strengh: number;
	thoughness: number;
    owner: Player;

    constructor(name: string, job: string, strengh: number, thoughness: number) {
        this.name = name;
        this.job = job;
        this.strengh = strengh;
        this.thoughness = thoughness;
        this.owner = null;
    }

    avatar() {
        return "assets/img/" + this.name + "-avatar.png";
    }

    full() {
        return "assets/img/" + this.name + "-full.png";
    }

    combat() {
        return "assets/img/" + this.name + "-mini.png";
    }

    setPlayer(owner: Player) {
        this.owner = owner;
    }

    unsetPlayer() {
        this.owner = null;
    }
}
