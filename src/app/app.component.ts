import { Component } from '@angular/core';
import { Player } from './player';
import { Hero } from './hero';
import { PLAYERS } from './mock-players';
import { HEROES } from './mock-heroes';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'The Math Game';
    players = PLAYERS;
    heroes = HEROES;
    startGame = false;
    errors = [];

    onAddNewPlayer(): void {
        this.players.push(new Player());
    }

    onSelectHero(hero: Hero) {
        for (let i = 0; i < this.players.length; i++) {
            let currentPlayer = this.players[i];
            if (currentPlayer.character != null) {
                if (currentPlayer.character.name == hero.name) {
                    // current hero of the player, so click again means unset
                    currentPlayer.character = null;
                    hero.unsetPlayer();
                    // We didn't allow direct change of hero
                }
            } else {
                hero.setPlayer(currentPlayer);
                currentPlayer.character = hero;
            }
        }
    }

    onStartGame(): void {
        // TODO Check players information are valid.
        let errors = [];
        for (let i = 0; i < this.players.length; i++) {
            let currentPlayer = this.players[0];
            if (currentPlayer.firstname.length < 1 ||
                currentPlayer.lastname.length < 1 ||
                currentPlayer.character == null)
                errors.push("Joueur n° " + (i+1) +
                    ": il faut au moins fournir ton nom, ton prénom et avoir choisit un avatar, pour continuer !");
        }
        if (errors.length > 0) {
            this.errors = errors;
        } else {
            this.startGame = true;
        }
    }
}
